library(shiny)
library(IQRtools)
library(PKPDmisc)
library(tidyverse)
library(ggthemes)
library(shinythemes)
library(lattice)

ui <- fluidPage(
    theme=shinytheme('simplex'),
    titlePanel('Onyvide DDI simulation'),
    sidebarLayout(
        sidebarPanel(
            fileInput("upload", "Upload a provided cpp file"),
            numericInput('Irino_dose', 'Onyvide dose(mg/m2, 80mg dose = 68.8 free base irinotecan)', 80),
            numericInput('Vacto_dose', 'Vactocertib dose(mg)', 200),
            numericInput('Irino_dur', 'Onyvide dosing duration(hr)', 1.5),
            numericInput('Irino_time', 'Onyvide input time(hr, min = -72 hrs)', 0, min = -72),
            selectInput("model", "Model select", c("EC50/Emax fixed" = "fixed",
                                                   "Emax fixed" = "no")),
            selectInput("race", "Asian ratio", c("0%" = "zero",
                                                        "20%" = "two",
                                                        "40%" = "four",
                                                        "60%" = "six",
                                                        "80%" = "eight",
                                                        "100%" = "ten")),
            actionButton('Run', 'Go simulation!')
        ),
        mainPanel(
            fluidRow(
                column(12,
                    plotOutput('plot1'),
                    plotOutput('plot2'),
                )),
            br(),
            br(),
            fluidRow(
                column(6, offset = 1,
                h3("NCA table"),
                tableOutput('result'),
                tableOutput('dose')
                )
            )
            )
        )
    )


server <- function(input, output, session) {
  data <- eventReactive(input$Run, {
    req(input$upload)
    
    model <- IQRmodel(input$upload$datapath)

    Irino_dose <- input$Irino_dose / 80 * 68.8 * 1.7 # 80mg/m2 every 2 weeks 90min infusion, 80mg dose = 68.8mg free base irinotecan
    vacto_dose <- input$Vacto_dose
    dose_time <- input$Irino_dur
    delayed_time <- 72
    changed_time <- input$Irino_time
    vacto <- data.frame(
        AMT = rep(vacto_dose, 3),
        TIME = c(delayed_time, 168 + delayed_time, 168 * 2 + delayed_time),
        II = rep(12, 3),
        ADDL = rep(9, 3),
        ADM = rep(1, 3),
        RATE = rep(0, 3)
    )

    onyvide <- data.frame(
        AMT = Irino_dose * 1000,
        TIME = delayed_time + changed_time,
        II = 24 * 7 * 2,
        ADDL = 2,
        ADM = 2,
        RATE = Irino_dose * 1000 / dose_time
    )

    dosinginfo <- c()
    for (i in 1:10) {
        single <- vacto %>% mutate(ID = i)
        dosinginfo <- dosinginfo %>% rbind(single)
    }
    for (i in 11:20) {
        combi <- rbind(vacto, onyvide) %>% mutate(ID = i)
        dosinginfo <- dosinginfo %>% rbind(combi)
    }

    pop_para_pre <- data.frame(
        ID = 1:20,
        SEX = rep(c(0, 1), 10)
       )
    
    race <- switch(input$race,
        "zero" = 0,
        "two" = 1,
        "four" = 2,
        "six" = 3,
        "eight" = 4,
        "ten" = 5
    )
    pop_para <- pop_para_pre %>% mutate(ASIAN = rep(c(rep(0,5-race), rep(1,race)), 4))
    eventTable_raw <- full_join(dosinginfo, pop_para, by = "ID")
    eventTable <- IQReventTable(eventTable_raw, regression = c("SEX", "ASIAN"))

    Sim_time <- seq(0, 24 * 21, 0.25)
    Blank_time <- expand.grid(TIME = Sim_time, ID = 1:20)

    if(input$model == 'fixed'){
        result_combi <- sim_IQRmodel(model, Sim_time, eventTable = eventTable, parameters = c(Kp_liver_V = 2.96, KA_V = 0.711, Vc_kg_V = 0.484, Vp_kg_V = 0.791, Q_kg_V = 0.336, SF_int_V = 1, EC50_3a4_uM_V = 7.01, Emax_3a4_V = 18.7, fup_I = 0.32))
    }else{
        result_combi <- sim_IQRmodel(model, Sim_time, eventTable = eventTable, parameters = c(Kp_liver_V = 2.57, KA_V = 0.724, Vc_kg_V = 0.518, Vp_kg_V = 0.759, Q_kg_V = 0.307, SF_int_V = 1, EC50_3a4_uM_V = 3.32, Emax_3a4_V = 18.7, fup_I = 0.32))
    }

    result_combi %>%
        as.data.frame() %>%
        select(ID, TIME, OUTPUT1, OUTPUT2, OUTPUT3) %>%
        full_join(Blank_time, by = c("ID", "TIME")) %>%
        arrange(ID, TIME) %>%
        mutate(
            OUTPUT1 = ifelse(is.na(OUTPUT1), 0, OUTPUT1),
            OUTPUT2 = ifelse(is.na(OUTPUT2), 0, OUTPUT2),
            OUTPUT3 = ifelse(is.na(OUTPUT3), 0, OUTPUT3),
            COMBI = ifelse(ID > 10, "combi", "single"),
            ID = ifelse(ID > 10, ID - 10, ID)
        ) %>%
        filter(TIME >= min(delayed_time, delayed_time + changed_time)) %>%
        mutate(TIME = (TIME - 72)/24)
    })

  output$result <- renderTable({
      nca_result <- c()
      nca_result_raw <- c()

        for (i in 1:10) {
            a1 <- data() %>%
                mutate(TIME = TIME * 24) %>%
                filter(TIME <= 24 * 14) %>%
                mutate(OUTPUT1 = ifelse(OUTPUT1 < 0.05, 0.05 / 2, OUTPUT1)) %>%
                filter(ID == i) %>%
                split(.$COMBI)

            nca <- map(a1, ~ .x %>%
                summarise(
                    AUC0_2week = auc_partial(.$TIME, .$OUTPUT1, range = c(0, 24 * 14)),
                    AUC0_1week = auc_partial(.$TIME, .$OUTPUT1, range = c(0, 24 * 7)),
                    AUC1_2week = auc_partial(.$TIME, .$OUTPUT1, range = c(24 * 7, 24 * 14)),
                    Cmax = max(.$OUTPUT1),
                    Cmin_120 = .x %>% filter(TIME == 24 * 5) %>% select(OUTPUT1) %>% as.numeric(),
                    Cmin_288 = .x %>% filter(TIME == 168 + 24 * 5) %>% select(OUTPUT1) %>% as.numeric(),
                    Cmin = .x %>% filter(TIME == 168) %>% pull(OUTPUT1) %>% unique()
                ), .id = "COMBI")
            nca_ratio <- nca[["combi"]] / nca[["single"]]
            nca_result_raw <- nca_ratio %>%
                round(4) %>%
                rbind(nca[["single"]] %>% round(4)) %>%
                rbind(nca[["combi"]] %>% round(4)) %>%
                mutate(Description = 1:3) %>%
                select(Description, everything()) %>%
                mutate(ID = i)
            nca_result <- nca_result %>% rbind(nca_result_raw)
                }
    nca_result %>%
        group_by(Description) %>%
        summarise_all(funs(mean)) %>%
        ungroup() %>%
        arrange(Description) %>%
        mutate(Description = c("Ratio", "Single", "Combi")) %>%
        select(-ID)

        })

  output$plot1 <- renderPlot({
      data() %>%
        ggplot(aes(x = TIME, y = OUTPUT1, color = COMBI)) +
        geom_line(size = 1) +
        theme_bw() +
        scale_color_calc() +
        labs(
            x = "Time(day)",
            y = "Vactocertib concentration(ng/mL)"
        )
  })

  output$plot2 <- renderPlot({
    data() %>%
        ggplot(aes(x = TIME, y = OUTPUT2, color = COMBI)) +
        geom_line(size = 1) +
        theme_bw() +
        scale_color_calc() +
        labs(
            x = "Time(day)",
            y = "Irinotecan concentration(ng/mL)"
        )
  })
  
  }

shinyApp(ui = ui, server = server)

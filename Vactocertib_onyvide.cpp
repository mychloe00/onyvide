********** MODEL NAME

Combination DDI model

********** MODEL NOTES

Vactocertib + Onyvide

********** MODEL STATES

d/dt(GUT_V) = - KA_V * GUT_V + 1000 * INPUT1
d/dt(PV_V) = KA_V * GUT_V * FA_V * Fg_V  - Qpv * PV_V / Vpv + Qpv * CENT_V / Vc_V
d/dt(LIV_V) = Qpv * PV_V / Vpv + Qha * CENT_V / Vc_V - Qh * LIV_V / Vh / Kp_liver_V * BP_V - SF_V * TVCL_V * hep_V
d/dt(CENT_V) = Qh * LIV_V / Vh / Kp_liver_V * BP_V - (Qha + Qpv) * CENT_V / Vc_V - Q_V * CENT_V / Vc_V + Q_V * PERI_V / Vp_V 
d/dt(PERI_V) = Q_V * CENT_V / Vc_V - Q_V * PERI_V / Vp_V


d/dt(CENT_I) = EQ_12 + EQ_13 + EQ_14 - K10*CENT_I + INPUT2
d/dt(CENT_SN) = -EQ_12 + EQ_23 + EQ_24 - K20*CENT_SN
d/dt(PERI_I) = -EQ_13 - EQ_23
d/dt(TRANSIT_I) = -EQ_14 - EQ_24 

d/dt(CYP3a4_ratio) = kdeg_3a4  * (1 + Emax_3a4_V * hep_V / (hep_V + EC50_3a4_V)) - (kdeg_3a4 + kinact_3a4_I * hep_I / (hep_I + kI_3a4_I * (1 + hep_SN / Ki_3a4_SN + hep_V / Ki_3a4_V)) + kinact_3a4_SN * hep_SN / (hep_SN + kI_3a4_SN * (1 + hep_I/Ki_3a4_I + hep_V/Ki_3a4_V))) * CYP3a4_ratio
d/dt(CYP3a4_ratio_intestine) = kdeg_3a4_intestine * (1 + Emax_3a4_V * portal_V / (portal_V + EC50_3a4_V)) - (kdeg_3a4_intestine + kinact_3a4_I * portal_I / (portal_I + kI_3a4_I * (1 + portal_SN / Ki_3a4_SN + portal_V / Ki_3a4_V)) + kinact_3a4_SN * portal_SN / (portal_SN + kI_3a4_SN * (1 + hep_I/Ki_3a4_I + hep_V/Ki_3a4_V))) * CYP3a4_ratio_intestine
d/dt(UGT1a1_ratio) = kdeg_ugt1a1  * (1 + Emax_ugt1a1_V * hep_V / (hep_V + EC50_ugt1a1_V)) - kdeg_ugt1a1 * UGT1a1_ratio
d/dt(UGT1a4_ratio) = kdeg_ugt1a4  * (1 + Emax_ugt1a4_V * hep_V / (hep_V + EC50_ugt1a4_V)) - kdeg_ugt1a4 * UGT1a4_ratio

GUT_V(0) = 0
PV_V(0) = 0
LIV_V(0) = 0
CENT_V(0) = 0
PERI_V(0) = 0

CENT_I(0) = 0
CENT_SN(0) = 0
PERI_I(0) = 0
TRANSIT_I(0) = 0

CYP3a4_ratio(0) = 1
CYP3a4_ratio_intestine(0) = 1
UGT1a1_ratio(0) = 1
UGT1a4_ratio(0) = 1

********** MODEL PARAMETERS

################# Physiology parameters

WT = 70
BSA = 1.71
ASIAN = 0
SEX = 0
Qpv_kg = 1.04
Vpv_kg = 0.015
Qha_kg = 0.37
Qh_kg = 1.41
Vh_kg = 0.034 # (L) hepatic volume
Qvilli_kg = 0.26


################# Vactocertib - Basic PK parameter

MW_V = 399.43 # # (g/mol) Molecular weight
logP_V = 3.31 # # Octanol/water partition coefficient
pKa_V = 8.07 # # unknown
fup_V = 0.039 # # fraction unbound in plasma
fu_gut_V = 1 # # fraction unbound in gut
fu_hep_V = 0.075 # # fraction unbound in liver (Dallphin)
Kp_liver_V = 10 # # Tissue-to-plasma parition coefficient *** ESTIMATED (Mice Kp value = 23.3)
BP_V = 0.58 # # Blood/plasma ratio *** Renewed
KA_V = 0.878 # # (/h) Absorption rate constant ***
Vc_kg_V = 0.275 # ESTIMATED ## (L/kg) Central volume ***
Vp_kg_V = 0.864 # ESTIMATED ## (L/kg) Peripheral volume ***
Q_kg_V = 0.403 # # Intercompartmental clearance ***
FA_V = 0.99 # # Fraction absorbed

Tlag1 = 0.1 # # Absorption lag time ***


############### Intrinsic clearance of Vactocertib

CLint_3a4_V = 42.51 # # Other intrinsic Clearance
CLint_3a4_intestine_V = 1.29 # # (L/h) Dallphin ESTIMATED ***
CLint_1a2_V = 3.14
CLint_2c8_V = 8.28
CLint_2c9_V = 3.19
CLint_2c19_V = 1.33
CLint_2d6_V = 41
CLint_ugt1a4_V = 2.71
CLint_ugt2b7_V = 0.85
Qgut_V = 16.6 # # (L/h)  Renewed with caco49
SF_int_V = 1.5 # #  TO BE ESTIMATED


############### DDI parameter of Vactocertib

kdeg_3a4 = 0.019 # # (/h) degradation rate constant
kdeg_3a4_intestine = 0.029 # # (/h) degradation rate constant
kdeg_2c8 = 0.08 # # (/h) degradation rate constant
kdeg_ugt1a1 = 0.02 # # (/h) degradation rate constant
kdeg_ugt1a4 = 0.02 # # (/h) degradation rate constant

fu_mic_3a4_V = 0.9
Emax_3a4_V = 18.7 ## 14.6
EC50_3a4_uM_V = 3.32 ## 1.8uM
fu_mic_ugt1a1_V = 0.9
Emax_ugt1a1_V = 8.51 ## 14.6
EC50_ugt1a1_uM_V = 4.3 ## 1.8uM
fu_mic_ugt1a4_V = 0.9
Emax_ugt1a4_V = 3.4 ## 14.6
EC50_ugt1a4_uM_V = 15.36 ## 1.8uM

Ki_3a4_uM_V = 2.2 # # Competitive inhibition constant (uM)
Ki_2c8_uM_V = 10.9 # # Value not included yet ***
fu_mic_2c8_V = 0.9 # # 0.1mg/ml then 0.9, 0.5mg/ml then 0.8
Ki_2d6_uM_V = 8.95
fu_mic_2d6_V = 0.9 # # 0.1mg/ml then 0.9, 0.5mg/ml then 0.8
Ki_ugt1a1_uM_V = 8.9


################# Irinotecan - Basic PK parameters

fup_I = 0.7 # # fraction unbound in plasma (30(0.7)~68(0.32) percent)
Kp_cap = 0.05
Vc_I = 4.09 ## (L) Central volume
Vp_I = 0.421 ## (L)
Q_I_week = 1.35 ## (L/week)
CL_I_week = 17.9 ## (L/week)

FM_delay = 0.629
FM_direct = 0.152

fup_SN = 0.05
CL_SN_week = 19800 ## (L/week)
V_SN = 4.09 ## (L) Central volume for metabolite (identical to Vc_I)
TRANS_SN_week = 2 ## (/week)

MW_I = 677.1
MW_SN = 392.4

################## Irinotecan - DDI parameters
Ki_3a4_uM_I = 129 # # Competitive inhibition constant (uM)
kI_3a4_uM_I = 24.1 # # Inhibitor concentration that supports half maximal rate of inactivation (uM)
kinact_3a4_I = 3.36 # # Maximal inactivation rate
fu_mic_3a4_I = 0.9 # # 0.1 mg/ml then 0.9, 0.5mg/ml then 0.8

Ki_3a4_uM_SN = 121 ## (uM)
fu_mic_3a4_SN = 0.9 ##
kI_3a4_uM_SN = 26 ## (uM)
kinact_3a4_SN = 6.12 ## 

Ki_2c9_uM_SN = 156
fu_mic_2c9_SN = 0.9

********** MODEL VARIABLES

# BW scaling for vactocertib

Qpv = Qpv_kg * WT
Vpv = Vpv_kg * WT
Qha = Qha_kg * WT
Qh = Qh_kg * WT
Vh = Vh_kg * WT
Qvilli = Qvilli_kg * WT

Vc_V = Vc_kg_V * WT
Vp_V = Vp_kg_V * WT
Q_V = Q_kg_V * WT


# Week -> hr scaling for Onyvide

Q_I = Q_I_week / 7 / 24
CL_I = CL_I_week / 7 / 24
CL_SN = CL_SN_week / 7 / 24
TRANS_SN = TRANS_SN_week / 7 / 24

CL_I_cov = CL_I * 1.204^(ASIAN)*0.799^(SEX)
Vc_I_cov = Vc_I * BSA^(0.573)/1.71*0.886^(SEX)
CL_SN_cov = CL_SN * 0.802^(SEX)

# uM to ng/mL scaling

## vactosertib

Ki_3a4_V = Ki_3a4_uM_V * MW_V * fu_mic_3a4_V
Ki_2c8_V = Ki_2c8_uM_V * MW_V * fu_mic_2c8_V
Ki_2d6_V = Ki_2d6_uM_V * MW_V * fu_mic_2d6_V
Ki_ugt1a1_V = Ki_ugt1a1_uM_V *MW_V* fu_mic_ugt1a1_V

EC50_3a4_V = EC50_3a4_uM_V * MW_V * fu_mic_3a4_V
EC50_ugt1a1_V = EC50_ugt1a1_uM_V * MW_V * fu_mic_ugt1a1_V
EC50_ugt1a4_V = EC50_ugt1a4_uM_V * MW_V * fu_mic_ugt1a4_V

## Onyvide

Ki_3a4_I = Ki_3a4_uM_I * MW_I * fu_mic_3a4_I
kI_3a4_I = kI_3a4_uM_I * MW_I * fu_mic_3a4_I

Ki_3a4_SN = Ki_3a4_uM_SN * MW_SN * fu_mic_3a4_SN
kI_3a4_SN = kI_3a4_uM_SN * MW_SN * fu_mic_3a4_SN
Ki_2c9_SN = Ki_2c9_uM_SN * MW_SN * fu_mic_2c9_SN


# Rate constant for Onyvide

KT = CL_I_cov/Vc_I
K13 = Q_I/Vc_I_cov
K31 = Q_I/Vp_I
K12 = FM_direct*KT
K14 = FM_delay*KT
K10 = (1 - FM_direct - FM_delay)*KT
K42 = TRANS_SN
K20 = CL_SN_cov/V_SN
K24 = 0
K41 = 0
K21 = 0
K23 = 0
K32 = 0


# Linear equation for Onyvide

EQ_12 = -(K12*CENT_I - K21*CENT_SN)
EQ_13 = -(K13*CENT_I - K31*PERI_I)
EQ_14 = -(K14*CENT_I - K41*TRANSIT_I)
EQ_23 = -(K23*CENT_SN - K32*PERI_I)
EQ_24 = -(K24*CENT_SN - K42*TRANSIT_I)


# Liver concentration, portal vein concentration for Vactosertib model
hep_V = LIV_V/Vh*fu_hep_V
portal_V = PV_V/Vpv*fu_gut_V

# Liver concentration, portal vein concentration for Onyvide model
hep_I = CENT_I*fup_I*Kp_cap
hep_SN = CENT_SN*fup_SN
portal_I = CENT_I*fup_I*Kp_cap
portal_SN = CENT_SN*fup_SN

SF_V = WT/70*SF_int_V


# Clearance of Vactosertib

CLint_other_V = CLint_1a2_V + CLint_2c19_V + CLint_2d6_V + CLint_2c8_V + CLint_2c9_V + CLint_ugt2b7_V  ## (L/h) Intrinsic clearance
TVCL_V = CLint_3a4_V/(1 + hep_I/Ki_3a4_I + hep_SN/Ki_3a4_SN)*CYP3a4_ratio + CLint_ugt1a4_V*UGT1a4_ratio + CLint_other_V

TVCL_intestine_V = CLint_3a4_intestine_V/(1 + portal_I/Ki_3a4_I + portal_SN/Ki_3a4_SN)*CYP3a4_ratio_intestine*SF_int_V
Fg_V = Qgut_V/(Qgut_V + TVCL_intestine_V*fu_gut_V)


# Output 

Cc_V = CENT_V/Vc_V/BP_V
OUTPUT1 = Cc_V
Cc_I = CENT_I/Vc_I
OUTPUT2 = Cc_I
Cc_SN = CENT_SN/V_SN
OUTPUT3 = Cc_SN

********** MODEL REACTIONS


********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL INITIAL ASSIGNMENTS
